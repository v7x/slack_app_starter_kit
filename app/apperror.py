class BaseAppError(Exception):
    '''
    Generic error which all custom errors should be derived from.
    '''

class AuthNotOKError(BaseAppError):
    '''
    For when slack api's auth.test command doesn't return ok.
    '''
