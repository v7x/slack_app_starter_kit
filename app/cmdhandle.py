COMMAND = {
    'default':0,
    'off':1,
    'help':2,
    'interactive':3,
    'about':4,
    'say':5
}

from sys import stdout

from flask import current_app as app

from .bot import rtmPost 

CMD_INVALID="""
            Invalid command. Use the command 'help' to see a list of commands.
            """
def cmdInvalid(channel):
    rtmPost(channel, CMD_INVALID)
    return ('', 200, None)

NOT_IMPLEMENTED="Command not yet implemented."

def notImplemented(channel):
    rtmPost(channel, NOT_IMPLEMENTED)
    return ('', 200, None)

from re import search

from .blocks import post_block

def cmdHandle(commandstr, infodict):
    '''
    Handle commands from users.
    '''
    user = infodict['user']
    
    if 'channel' in infodict.keys():
        channel = infodict['channel']
    else:
        channel = user

    command = commandstr.split(None, 1)
    cmdArgs = None

    cmdStr = command[0].lower()
    if len(command) > 1:
        cmdArgs = command[1]
    
    if cmdStr not in COMMAND.keys():
        app.logger.debug("Command not found.")
        return cmdInvalid(channel)

    cmd = COMMAND[cmdStr]

    if cmd is COMMAND['off'] and 0 <= uauth <= 1:
        stdout.write("App going down.")
        app.app_context().pop()
    
    elif cmd is COMMAND['help']:
        app.logger.debug("Handling 'help' command.")
        return post_block(channel, 'json/help.json')

    elif cmd is COMMAND['about']:
        app.logger.debug("Handling 'about' command.")
        return post_block(channel, 'json/about.json')

    elif cmd is COMMAND['interactive']:
        app.logger.debug("Handling 'interactive' command.")
        return post_block(channel, 'json/interactive.json')

    elif cmd is COMMAND['say']:
        if cmdArgs is not None:
            rtmPost(channel, cmdArgs)
        else:
            rtmPost(channel, "Give me something to say!")

        return ('', 200, None)

    else:
        app.logger.debug("Handling invalid command.")
        return cmdInvalid(channel)
