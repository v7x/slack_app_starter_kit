
from os import makedirs, environ
from os.path import join, exists
from sys import stderr, stdout
from logging import basicConfig

from flask import Flask, g, request
from slackeventsapi import SlackEventAdapter

from .bot import chatlistener
from .interactive import interactivity_webhook

"""
The function create_app is based on the one featured in the flask tutorial,
which can be found here: http://flask.pocoo.org/docs/1.0/tutorial/factory/
"""
def create_app():
    basicConfig(format="[%(asctime)s][%(levelname)s]: %(message)s",
            level=environ['LOG_LVL'], stream=stdout)

    app = Flask(__name__, instance_path='/app')

    app.secret_key = environ['APP_SECRET']

    #this just confirms the instance path exists
    try:
        makedirs(app.instance_path)
    except OSError:
        pass
    
    """
    Defines the domain's root endpoint
    """
    @app.route('/', methods=['GET', 'POST'])
    def homepage():
        return "It works!"
    
    """
    Defines the enpoint where Slack will send any payloads originating from
    interactive components of block messages.
    """
    @app.route('/slack', methods=['GET', 'POST'])
    def interactivity():
        return interactivity_webhook(request)

    slack_events_adapter = SlackEventAdapter(environ['SIGNING_SECRET'], 
            '/slack/events', app)
    
    """
    Define the endpoint where slack will send any events you have designated
    (in our case, 'app_mention').
    """
    @slack_events_adapter.on("app_mention")
    def chat_listener(event):
        return chatlistener(event['event'])

    return app
