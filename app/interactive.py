from json import loads, dumps

from flask import current_app as app
from requests import post

def interactivity_webhook(request):
    app.logger.debug("Interactivity webhook received.")
    payload = loads(request.form['payload'])
    paykeys = payload.keys()

    if 'type' in paykeys:
        if payload['type'] == "url_verification" and "challenge" in paykeys:
            return (payload['challenge'], 200, None)

        elif payload['type'] == "block_actions":
            user = payload['user']['id']
            action = payload['actions'][0]
            return handle_action(user, payload['response_url'], action,
                    payload['channel'])
    else:
        return ('', 403, None)

RESPONSE_ERR = "Request to slack returned error {e}, response is '{r}'."

def handle_action(user, url, action, channeldata):
    channel = channeldata['id']
    app.logger.debug("Action received from user {u}.".format(u=user))
    id = action['action_id']

    if id == 'click':
        message = {'text': "Hello, Slack!"}
        response = post(url, 
                json = message, 
                headers={'Content-Type': 'application/json'}
            )

        if response.status_code != 200:
            app.logger.warning(RESPONSE_ERR.format(e=response.status_code,
                        r=response.text))

        return ('', response.status_code, None)

    else:
        app.logger.debug("Invalid action received.")
        return ('', 404, None)
